#!/usr/bin/env sh

function semverParseInto() {
  local RE='[^0-9]*\([0-9]*\)[.]\([0-9]*\)[.]\([0-9]*\)\([0-9A-Za-z-]*\)';
  eval $2=`echo $1 | sed -e "s#$RE#\1#"`;
  eval $3=`echo $1 | sed -e "s#$RE#\2#"`;
  eval $4=`echo $1 | sed -e "s#$RE#\3#"`;
  eval $5=`echo $1 | sed -e "s#$RE#\4#"`;
}

if [ -z "$1" ] || [ -z "$2" ]; then
  echo "Two arguments required";
  exit 1;
fi

if ! echo "$2" | grep -Eq '[^0-9]*([0-9]*)[.]([0-9]*)[.]([0-9]*)([0-9A-Za-z-]*)'; then
  echo "Invalid semantic version";
  exit 1;
fi

MAJOR=0
MINOR=0
PATCH=0
SPECIAL=""
semverParseInto $2 MAJOR MINOR PATCH SPECIAL

case $1 in
  major) echo "$MAJOR" ;;
  minor) echo "$MAJOR.$MINOR" ;;
  patch) echo "$MAJOR.$MINOR.$PATCH" ;;
  *) echo "Unknown argument passed: $1"; exit 1 ;;
esac