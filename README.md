# @groovox/hasura

[![License][license_md]][license]
[![GitLab CI][gitlab_ci]][gitlab]
[![Docker Pull][docker_pull]][docker]
[![Docker Star][docker_star]][docker]
[![Docker Size][docker_size]][docker]
[![Docker Layer][docker_layer]][docker]

This is the metadata server using [Hasura][hasura]. It contains migrations, seeds and metadata used for the server.

You can reference the [documentation] from Hasura for detail configuration.

## Getting Started

```
docker run groovox/hasura -e HASURA_GRAPHQL_DATABASE_URL:<DATABASE_URL> -p 8080:8080
```

or `docker-compose.yaml`.

```yaml
version: "3.6"
services:
  postgres:
    image: postgres:12
    restart: always
    volumes:
      - db_data:/var/lib/postgresql/data
    environment:
      POSTGRES_PASSWORD: postgrespassword
  groovox:
    image: groovox/hasura:1.1.0
    restart: always
    environment:
      HASURA_GRAPHQL_DATABASE_URL: postgres://postgres:postgrespassword@postgres:5432/postgres
      HASURA_GRAPHQL_ENABLE_CONSOLE: "true"
    ports:
      - 8080:8080
    depends_on:
      - postgres
volumes:
  db_data:
```

Note that this will automatically migrate your database and accessable at port 8080.

## Requirement

- Docker
- PostgreSQL 9.5+

## Frequently Asked Question

**Q. How do I manages that database migratation manually?**

A. You can use the [hasura/graphql-engine][docker] docker. Make use you update metadata after each migratations.

**Q. How do I secure the API?**

A. You refer to the [documentation][auth] from Hasura. You can use a simple admin secret or connect to other OAuth services.

[hasura]: https://hasura.io/
[documentation]: https://hasura.io/docs/1.0/graphql/core/index.html
[docker]: https://hub.docker.com/r/hasura/graphql-engine
[auth]: https://hasura.io/docs/1.0/graphql/core/auth/index.html
[docker]: https://hub.docker.com/r/groovox/hasura
[docker_pull]: https://img.shields.io/docker/pulls/groovox/hasura.svg
[docker_star]: https://img.shields.io/docker/stars/groovox/hasura.svg
[docker_size]: https://img.shields.io/microbadger/image-size/groovox/hasura.svg
[docker_layer]: https://img.shields.io/microbadger/layers/groovox/hasura.svg
[license]: https://gitlab.com/groovox/hasura/blob/master/LICENSE
[license_md]: https://img.shields.io/badge/license-Apache--2.0-green.svg
[gitlab]: https://gitlab.com/groovox/hasura/pipelines
[gitlab_ci]: https://gitlab.com/groovox/hasura/badges/master/pipeline.svg
[project]: https://gitlab.com/groovox/hasura
