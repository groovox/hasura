CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE genre (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  name TEXT UNIQUE NOT NULL
);

CREATE TABLE collection (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  name TEXT UNIQUE NOT NULL
);

CREATE TABLE studio (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  name TEXT UNIQUE NOT NULL
);

CREATE TABLE country (
  code TEXT PRIMARY KEY,
  name TEXT UNIQUE NOT NULL
);

CREATE TABLE style (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  name TEXT UNIQUE NOT NULL
);

CREATE TABLE mood (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  name TEXT UNIQUE NOT NULL
);

CREATE TABLE person (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  name TEXT NOT NULL,
  name_sort TEXT DEFAULT NULL,
  name_match TEXT UNIQUE NOT NULL,
  image_cover TEXT DEFAULT NULL
);

CREATE TABLE staff (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  name TEXT NOT NULL
);

CREATE TABLE artist (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  person_id uuid NOT NULL,
  summary TEXT NOT NULL DEFAULT '',
  image_cover TEXT DEFAULT NULL,
  image_background TEXT DEFAULT NULL,
  FOREIGN KEY (person_id) REFERENCES person(id) ON DELETE CASCADE
);

CREATE TABLE artist_similar (
  artist1_id uuid NOT NULL,
  artist2_id uuid NOT NULL,
  FOREIGN KEY (artist1_id) REFERENCES artist(id) ON DELETE CASCADE,
  FOREIGN KEY (artist2_id) REFERENCES artist(id) ON DELETE CASCADE,
  PRIMARY KEY (artist1_id, artist2_id)
);

CREATE TABLE artist_genre (
  artist_id uuid NOT NULL,
  genre_id uuid NOT NULL,
  index INTEGER NOT NULL,
  FOREIGN KEY (artist_id) REFERENCES artist(id) ON DELETE CASCADE,
  FOREIGN KEY (genre_id) REFERENCES genre(id) ON DELETE CASCADE,
  PRIMARY KEY (artist_id, index),
  UNIQUE (artist_id, genre_id)
);

CREATE TABLE artist_collection (
  artist_id uuid NOT NULL,
  collection_id uuid NOT NULL,
  index INTEGER NOT NULL,
  FOREIGN KEY (artist_id) REFERENCES artist(id) ON DELETE CASCADE,
  FOREIGN KEY (collection_id) REFERENCES collection(id) ON DELETE CASCADE,
  PRIMARY KEY (artist_id, index),
  UNIQUE (artist_id, collection_id)
);

CREATE TABLE artist_country (
  artist_id uuid NOT NULL,
  country_code TEXT NOT NULL,
  index INTEGER NOT NULL,
  FOREIGN KEY (artist_id) REFERENCES artist(id) ON DELETE CASCADE,
  FOREIGN KEY (country_code) REFERENCES country(code) ON DELETE CASCADE,
  PRIMARY KEY (artist_id, index),
  UNIQUE (artist_id, country_code)
);

CREATE TABLE artist_style (
  artist_id uuid NOT NULL,
  style_id uuid NOT NULL,
  index INTEGER NOT NULL,
  FOREIGN KEY (artist_id) REFERENCES artist(id) ON DELETE CASCADE,
  FOREIGN KEY (style_id) REFERENCES style(id) ON DELETE CASCADE,
  PRIMARY KEY (artist_id, index),
  UNIQUE (artist_id, style_id)
);

CREATE TABLE artist_mood (
  artist_id uuid NOT NULL,
  mood_id uuid NOT NULL,
  index INTEGER NOT NULL,
  FOREIGN KEY (artist_id) REFERENCES artist(id) ON DELETE CASCADE,
  FOREIGN KEY (mood_id) REFERENCES mood(id) ON DELETE CASCADE,
  PRIMARY KEY (artist_id, index),
  UNIQUE (artist_id, mood_id)
);

CREATE TABLE album (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  name TEXT NOT NULL,
  name_sort TEXT DEFAULT NULL,
  name_match TEXT UNIQUE NOT NULL,
  rating NUMERIC(3, 1) DEFAULT NULL,
  summary TEXT NOT NULL DEFAULT '',
  image_cover TEXT NOT NULL,
  image_background TEXT DEFAULT NULL,
  aired DATE DEFAULT NULL,
  artist_id uuid NOT NULL,
  FOREIGN KEY (artist_id) REFERENCES artist(id) ON DELETE CASCADE
);

CREATE TABLE album_genre (
  album_id uuid NOT NULL,
  genre_id uuid NOT NULL,
  index INTEGER NOT NULL,
  FOREIGN KEY (album_id) REFERENCES album(id) ON DELETE CASCADE,
  FOREIGN KEY (genre_id) REFERENCES genre(id) ON DELETE CASCADE,
  PRIMARY KEY (album_id, index),
  UNIQUE (album_id, genre_id)
);

CREATE TABLE album_collection (
  album_id uuid NOT NULL,
  collection_id uuid NOT NULL,
  index INTEGER NOT NULL,
  FOREIGN KEY (album_id) REFERENCES album(id) ON DELETE CASCADE,
  FOREIGN KEY (collection_id) REFERENCES collection(id) ON DELETE CASCADE,
  PRIMARY KEY (album_id, index),
  UNIQUE (album_id, collection_id)
);

CREATE TABLE album_studio (
  album_id uuid NOT NULL,
  studio_id uuid NOT NULL,
  index INTEGER NOT NULL,
  FOREIGN KEY (album_id) REFERENCES album(id) ON DELETE CASCADE,
  FOREIGN KEY (studio_id) REFERENCES studio(id) ON DELETE CASCADE,
  PRIMARY KEY (album_id, index),
  UNIQUE (album_id, studio_id)
);

CREATE TABLE album_country (
  album_id uuid NOT NULL,
  country_code TEXT NOT NULL,
  index INTEGER NOT NULL,
  FOREIGN KEY (album_id) REFERENCES album(id) ON DELETE CASCADE,
  FOREIGN KEY (country_code) REFERENCES country(code) ON DELETE CASCADE,
  PRIMARY KEY (album_id, index),
  UNIQUE (album_id, country_code)
);

CREATE TABLE album_style (
  album_id uuid NOT NULL,
  style_id uuid NOT NULL,
  index INTEGER NOT NULL,
  FOREIGN KEY (album_id) REFERENCES album(id) ON DELETE CASCADE,
  FOREIGN KEY (style_id) REFERENCES style(id) ON DELETE CASCADE,
  PRIMARY KEY (album_id, index),
  UNIQUE (album_id, style_id)
);

CREATE TABLE album_mood (
  album_id uuid NOT NULL,
  mood_id uuid NOT NULL,
  index INTEGER NOT NULL,
  FOREIGN KEY (album_id) REFERENCES album(id) ON DELETE CASCADE,
  FOREIGN KEY (mood_id) REFERENCES mood(id) ON DELETE CASCADE,
  PRIMARY KEY (album_id, index),
  UNIQUE (album_id, mood_id)
);

CREATE TABLE track (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  name TEXT NOT NULL,
  name_sort TEXT DEFAULT NULL,
  rating NUMERIC(3, 1) DEFAULT NULL
);

CREATE TABLE track_genre (
  track_id uuid NOT NULL,
  genre_id uuid NOT NULL,
  index INTEGER NOT NULL,
  FOREIGN KEY (track_id) REFERENCES track(id) ON DELETE CASCADE,
  FOREIGN KEY (genre_id) REFERENCES genre(id) ON DELETE CASCADE,
  PRIMARY KEY (track_id, index),
  UNIQUE (track_id, genre_id)
);

CREATE TABLE track_mood (
  track_id uuid NOT NULL,
  mood_id uuid NOT NULL,
  index INTEGER NOT NULL,
  FOREIGN KEY (track_id) REFERENCES track(id) ON DELETE CASCADE,
  FOREIGN KEY (mood_id) REFERENCES mood(id) ON DELETE CASCADE,
  PRIMARY KEY (track_id, index),
  UNIQUE (track_id, mood_id)
);


CREATE TABLE album_track (
  album_id uuid NOT NULL,
  track_id uuid NOT NULL,
  index INTEGER NOT NULL,
  disk INTEGER NOT NULL,
  FOREIGN KEY (track_id) REFERENCES track(id) ON DELETE CASCADE,
  FOREIGN KEY (album_id) REFERENCES album(id) ON DELETE CASCADE,
  PRIMARY KEY (track_id, disk, index),
  UNIQUE (album_id, track_id)
);

CREATE TABLE movie (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  name TEXT NOT NULL,
  name_sort TEXT DEFAULT NULL,
  name_original TEXT[] DEFAULT NULL,
  name_match TEXT UNIQUE NOT NULL,
  content_rating TEXT NOT NULL,
  aired DATE DEFAULT NULL,
  tagline TEXT NOT NULL DEFAULT '',
  rating NUMERIC(3, 1) DEFAULT NULL,
  summary TEXT NOT NULL DEFAULT '',
  image_cover TEXT DEFAULT NULL,
  image_background TEXT DEFAULT NULL
);

CREATE TABLE movie_genre (
  movie_id uuid NOT NULL,
  genre_id uuid NOT NULL,
  index INTEGER NOT NULL,
  FOREIGN KEY (movie_id) REFERENCES movie(id) ON DELETE CASCADE,
  FOREIGN KEY (genre_id) REFERENCES genre(id) ON DELETE CASCADE,
  PRIMARY KEY (movie_id, index),
  UNIQUE (movie_id, genre_id)
);

CREATE TABLE movie_collection (
  movie_id uuid NOT NULL,
  collection_id uuid NOT NULL,
  index INTEGER NOT NULL,
  FOREIGN KEY (movie_id) REFERENCES movie(id) ON DELETE CASCADE,
  FOREIGN KEY (collection_id) REFERENCES collection(id) ON DELETE CASCADE,
  PRIMARY KEY (movie_id, index),
  UNIQUE (movie_id, collection_id)
);

CREATE TABLE movie_studio (
  movie_id uuid NOT NULL,
  studio_id uuid NOT NULL,
  index INTEGER NOT NULL,
  FOREIGN KEY (movie_id) REFERENCES movie(id) ON DELETE CASCADE,
  FOREIGN KEY (studio_id) REFERENCES studio(id) ON DELETE CASCADE,
  PRIMARY KEY (movie_id, index),
  UNIQUE (movie_id, studio_id)
);

CREATE TABLE movie_country (
  movie_id uuid NOT NULL,
  country_code TEXT NOT NULL,
  index INTEGER NOT NULL,
  FOREIGN KEY (movie_id) REFERENCES movie(id) ON DELETE CASCADE,
  FOREIGN KEY (country_code) REFERENCES country(code) ON DELETE CASCADE,
  PRIMARY KEY (movie_id, index),
  UNIQUE (movie_id, country_code)
);

CREATE TABLE movie_staff (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  movie_id uuid NOT NULL,
  staff_id uuid NOT NULL,
  person_id uuid NOT NULL,
  index INTEGER NOT NULL,
  custom_staff_name TEXT DEFAULT NULL,
  FOREIGN KEY (movie_id) REFERENCES movie(id) ON DELETE CASCADE,
  FOREIGN KEY (staff_id) REFERENCES staff(id) ON DELETE CASCADE,
  FOREIGN KEY (person_id) REFERENCES person(id) ON DELETE CASCADE,
  UNIQUE (movie_id, staff_id, index),
  UNIQUE (movie_id, staff_id, person_id)
);

CREATE TABLE show (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  name TEXT NOT NULL,
  name_sort TEXT DEFAULT NULL,
  name_original TEXT[] DEFAULT NULL,
  name_match TEXT UNIQUE NOT NULL,
  content_rating TEXT NOT NULL,
  aired DATE DEFAULT NULL,
  tagline TEXT NOT NULL DEFAULT '',
  rating NUMERIC(3, 1) DEFAULT NULL,
  summary TEXT NOT NULL DEFAULT '',
  image_cover TEXT DEFAULT NULL,
  image_background TEXT DEFAULT NULL
);

CREATE TABLE show_genre (
  show_id uuid NOT NULL,
  genre_id uuid NOT NULL,
  index INTEGER NOT NULL,
  FOREIGN KEY (show_id) REFERENCES show(id) ON DELETE CASCADE,
  FOREIGN KEY (genre_id) REFERENCES genre(id) ON DELETE CASCADE,
  PRIMARY KEY (show_id, index),
  UNIQUE (show_id, genre_id)
);

CREATE TABLE show_collection (
  show_id uuid NOT NULL,
  collection_id uuid NOT NULL,
  index INTEGER NOT NULL,
  FOREIGN KEY (show_id) REFERENCES show(id) ON DELETE CASCADE,
  FOREIGN KEY (collection_id) REFERENCES collection(id) ON DELETE CASCADE,
  PRIMARY KEY (show_id, index),
  UNIQUE (show_id, collection_id)
);

CREATE TABLE show_season (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  show_id uuid NOT NULL,
  name TEXT NOT NULL,
  season INTEGER NOT NULL,
  summary TEXT NOT NULL DEFAULT '',
  FOREIGN KEY (show_id) REFERENCES show(id) ON DELETE CASCADE,
  UNIQUE (show_id, season)
);

CREATE TABLE show_staff (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  show_id uuid NOT NULL,
  staff_id uuid NOT NULL,
  person_id uuid NOT NULL,
  index INTEGER NOT NULL,
  custom_staff_name TEXT DEFAULT NULL,
  FOREIGN KEY (show_id) REFERENCES show(id) ON DELETE CASCADE,
  FOREIGN KEY (staff_id) REFERENCES staff(id) ON DELETE CASCADE,
  FOREIGN KEY (person_id) REFERENCES person(id) ON DELETE CASCADE,
  UNIQUE (show_id, staff_id, index),
  UNIQUE (show_id, staff_id, person_id)
);

CREATE TABLE episode(
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  name TEXT[] NOT NULL,
  name_sort TEXT DEFAULT NULL,
  name_original TEXT[] DEFAULT NULL,
  content_rating TEXT NOT NULL,
  aired DATE DEFAULT NULL,
  rating NUMERIC(3, 1) DEFAULT NULL,
  summary TEXT NOT NULL DEFAULT '',
  image_cover TEXT DEFAULT NULL
);

CREATE TABLE episode_staff (
  id uuid PRIMARY KEY DEFAULT uuid_generate_v4(),
  episode_id uuid NOT NULL,
  staff_id uuid NOT NULL,
  person_id uuid NOT NULL,
  index INTEGER NOT NULL,
  custom_staff_name TEXT DEFAULT NULL,
  FOREIGN KEY (episode_id) REFERENCES episode(id) ON DELETE CASCADE,
  FOREIGN KEY (staff_id) REFERENCES staff(id) ON DELETE CASCADE,
  FOREIGN KEY (person_id) REFERENCES person(id) ON DELETE CASCADE,
  UNIQUE (episode_id, staff_id, index),
  UNIQUE (episode_id, staff_id, person_id)
);
