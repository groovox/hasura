ALTER TABLE collection
  DROP summary,
  DROP image_cover,
  DROP image_background;