ALTER TABLE collection
  ADD summary TEXT NOT NULL DEFAULT '',
  ADD image_cover TEXT DEFAULT NULL,
  ADD image_background TEXT DEFAULT NULL;